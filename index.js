class App {
    update() {
        this.render();
    }

    render() {
        let root = document.getElementById('root');
        root.innerHTML = '';
        root.appendChild(
            container({}, [
                row({}, [
                    col({xs: 12, md: 6, lg: 3}, [
                        Book.getForm()
                    ]),
                    col({xs: 12, md: 6, lg: 9}, [
                        new BookList(db.findAll()).render()
                    ])
                ])
            ])
        );
    }
}


let db = new DB();

let app = new App();

db.registerObserver(app);

app.render();