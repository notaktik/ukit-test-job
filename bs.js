const container = (options, children) => {
    let container = document.createElement('div');

    container.classList.add('container');
    options.hasOwnProperty('fluid') && opts['fluid'] === true && container.classList.add('fluid');

    children.forEach(child => container.appendChild(child));
    return container;
};

const row = (options, children) => {
    let row = document.createElement('div');

    row.classList.add('row');
    options.hasOwnProperty('classes') && options['classes'] instanceof Array && options['classes'].forEach(className => row.classList.add(className));

    children.forEach(child => row.appendChild(child));
    return row;
};

const col = (options, children) => {
    let col = document.createElement('div');

    col.classList.add('col');
    options.hasOwnProperty('xs') && !isNaN(+options['xs']) && col.classList.add(`col-${options.xs}`);
    options.hasOwnProperty('sm') && !isNaN(+options['sm']) && col.classList.add(`col-sm-${options.sm}`);
    options.hasOwnProperty('md') && !isNaN(+options['md']) && col.classList.add(`col-md-${options.md}`);
    options.hasOwnProperty('lg') && !isNaN(+options['lg']) && col.classList.add(`col-lg-${options.lg}`);
    options.hasOwnProperty('xl') && !isNaN(+options['xl']) && col.classList.add(`col-xl-${options.xl}`);

    children.forEach(child => col.appendChild(child));
    return col;
};

const flex = (options, children) => {
    let flex = document.createElement('div');

    flex.classList.add('d-flex');
    options.hasOwnProperty('classes') && options['classes'] instanceof Array && options['classes'].forEach(className => flex.classList.add(className));

    children.forEach(child => flex.appendChild(child));
    return flex;
};