const SaveBookEvent = new Event('savebook');

window.addEventListener('savebook', function (event) {
    let form = document.forms.namedItem(Book.formId()),
        name = form.querySelector('#name'),
        date = form.querySelector('#date'),
        author = form.querySelector('#author'),
        pages = form.querySelector('#pages');

    let book = new Book({
        id: window.activeBookId,
        name: name.value,
        date: new Date(date.value),
        author: author.value,
        pages: pages.value
    });
    db.save(book);
    delete window.activeBookId
});

const ShowBookEvent = new Event('showbook');

window.addEventListener('showbook', function (event) {
    let form = document.forms.namedItem(Book.formId()),
        name = form.querySelector('#name'),
        date = form.querySelector('#date'),
        author = form.querySelector('#author'),
        pages = form.querySelector('#pages');
    let { book } = event;
    window.activeBookId = book.id;
    name.value = book.name;
    date.value = `${book.date.getFullYear()}-${('0' + (book.date.getMonth() + 1)).slice(-2)}-${('0' + (book.date.getDate())).slice(-2)}`;
    author.value = book.author;
    pages.value = book.pages;
});