class ElementBuilder {
    static createTextDiv({className = '', text}) {
        let el = document.createElement('div');
        el.innerText = text || '';
        if (!!className) {
            el.classList.add(className);
        }
        return el
    }

    static createInput({name, type, label}) {
        let container = document.createElement('div');
        container.classList.add('input-container');

        let head = document.createElement('label');
        head.setAttribute('for', name);
        head.innerText = label;

        let input = document.createElement('input');
        input.setAttribute('type', type);
        input.setAttribute('id', name);
        input.setAttribute('name', name);

        container.appendChild(head);
        container.appendChild(input);
        return container;
    }

    static createButton({label, onclick = function() {}}) {
        let button = document.createElement('button');
        button.innerText = label;
        button.addEventListener('click', onclick);
        return button;
    }
}