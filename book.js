class Book {
    constructor(book) {
        if (book.hasOwnProperty('id')) {
            this.id = book.id;
        }
        if (book.hasOwnProperty('name')) {
            this.name = book.name;
        }
        if (book.hasOwnProperty('date')) {
            this.date = new Date(book.date)
        }
        if (book.hasOwnProperty('author')) {
            this.author = book.author
        }
        if (book.hasOwnProperty('pages')) {
            this.pages = book.pages
        }
    }

    static formId() { return 'add-book-form' }

    static getForm() {
        let form = document.createElement('form');
        form.appendChild(ElementBuilder.createInput({
            name: 'name',
            type: 'text',
            label: 'Название'
        }));
        form.appendChild(ElementBuilder.createInput({
            name: 'date',
            type: 'date',
            label: 'Дата создания'
        }));
        form.appendChild(ElementBuilder.createInput({
            name: 'author',
            type: 'text',
            label: 'Автор'
        }));
        form.appendChild(ElementBuilder.createInput({
            name: 'pages',
            type: 'number',
            label: 'Количество страниц'
        }));
        form.appendChild(ElementBuilder.createButton({
            label: 'Сохранить'
        }));

        form.setAttribute("id", Book.formId());
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            dispatchEvent(SaveBookEvent);
        });
        return form;
    }

    setName(name) {
        this.name = name;
    }

    setDate(date) {
        this.date = date;
    }

    setAuthor(author) {
        this.author = author;
    }

    setPages(pages) {
        this.pages = +pages;
    }

    render() {
        let el = document.createElement('div');
        el.classList.add('book-container');

        el.appendChild(
            ElementBuilder.createTextDiv({
                text: `Название: ${this.name}`,
                className: 'book-property'
            })
        );
        el.appendChild(
            ElementBuilder.createTextDiv({
                text: `Дата написания: ${this.date.toLocaleDateString('RU')}`,
                className: 'book-property'
            })
        );
        el.appendChild(
            ElementBuilder.createTextDiv({
                text: `Автор: ${this.author}`,
                className: 'book-property'
            })
        );
        el.appendChild(
            ElementBuilder.createTextDiv({
                text: `Количество страниц: ${this.pages}`,
                className: 'book-property'
            })
        );
        el.appendChild(
            flex({classes: ['justify-content-around']}, [
                ElementBuilder.createButton({
                    label: 'Редактировать',
                    onclick: () => {
                        let evt = ShowBookEvent;
                        evt.book = this;
                        dispatchEvent(evt);
                    }
                }),
                ElementBuilder.createButton({
                    label: 'Удалить',
                    onclick: () => {
                        if (confirm(`Удалить книгу ${this.name}`)) {
                            db.remove(this.id);
                        }
                    }
                })
            ])
        );

        return el;
    }
}

class BookList {
    constructor(books) {
        this.books = books
    }

    render() {
        let el = document.createElement('div');
        el.setAttribute('id', 'booklist');
        el.appendChild(
            row(
                {classes: ['justify-content-center']},
                this.books.map(
                    book =>
                        col({xs: 12, sm: 8, md: 12, lg: 6, xl: 5}, [
                            book.render()
                        ])
                )
            )
        );
        return el;
    }
}