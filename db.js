class DB {
    constructor() {
        this._db = localStorage;
        this.pre = 'book_';
        this.observers = new Set();
    }

    registerObserver(observer) {
        this.observers.add(observer);
    }

    removeObserver(observer) {
        this.observers.delete(observer);
    }

    notifyObservers() {
        [...this.observers].map(observer => observer.update(this._db));
    }

    getInc() {
        return Object.keys(this._db).length ? Object.keys(this._db).map(key => +(key.match(/\d+/))).sort().pop() + 1 : 1;
    }

    find(id) {
        return new Book(this._db.getItem(this.pre + id));
    }

    findAll() {
        return Object.entries(this._db)
            .filter(item => new RegExp(`^${this.pre}`).test(item[0]))
            .map(item => new Book(JSON.parse(item[1])));
    }

    save(book) {
        if (!(book instanceof Book)) {
            throw new Error();
        }

        if (book.id) {
            let newBook = this.find(book.id);
            this._db.setItem(this.pre + book.id, JSON.stringify(Object.assign(newBook, book)));
        } else {
            let id = this.getInc();
            this._db.setItem(this.pre + id, JSON.stringify(Object.assign({}, book, {id: this.getInc()})));
        }
        this.notifyObservers();
    }

    remove(id) {
        this._db.removeItem(this.pre + id);
        this.notifyObservers();
    }
}